module Gitlab
  module Triage
    module Action
      class Base
        attr_reader :name, :type, :rule, :resources, :net

        def initialize(name:, type:, rule:, resources:, net:)
          @name = name
          @type = type
          @rule = rule
          @resources = resources
          @net = net
        end
      end
    end
  end
end
