# frozen_string_literal: true

require_relative '../base'
require_relative '../../command_builders/text_content_builder'

module Gitlab
  module Triage
    module Action
      class IssueBuilder
        attr_reader :title

        def initialize(action, resources, net)
          @title = action[:title]
          @item_template = action[:item]
          @summary_template = action[:summary]
          @resources = resources
          @net = net
        end

        def description
          return '' unless @summary_template

          @description ||= CommandBuilders::TextContentBuilder.new(
            @summary_template, resource: summary_resource, net: @net)
            .build_command
        end

        def valid?
          title =~ /\S+/
        end

        private

        def summary_resource
          @summary_resource ||= {
            'items' => items,
            'title' => title
          }
        end

        def items
          return '' unless @item_template

          @items ||= @resources.map(&method(:build_item)).join("\n")
        end

        def build_item(resource)
          CommandBuilders::TextContentBuilder.new(
            @item_template, resource: resource, net: @net)
            .build_command.chomp
        end
      end
    end
  end
end
