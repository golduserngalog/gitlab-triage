require 'active_support/all'
require 'net/protocol'

require_relative 'retryable'
require_relative 'ui'

module Gitlab
  module Triage
    class Network
      include Retryable

      attr_reader :options

      def initialize(adapter, options = {})
        @adapter = adapter
        @options = options
        @cache = Hash.new { |hash, key| hash[key] = {} }
      end

      def query_api_cached(token, url)
        @cache.dig(token, url) || @cache[token][url] = query_api(token, url)
      end

      def query_api(token, url)
        response = {}
        resources = []

        begin
          print '.'

          response = execute_with_retry(Net::ReadTimeout) do
            @adapter.get(token, response.fetch(:next_page_url) { url })
          end

          results = response.delete(:results)

          case results
          when Array
            resources.concat(results)
          else
            resources << results
          end
        end while response.delete(:more_pages)

        resources.map!(&:with_indifferent_access)
      rescue Net::ReadTimeout
        []
      end

      def post_api(token, url, body)
        execute_with_retry(Net::ReadTimeout) do
          @adapter.post(token, url, body)
        end

      rescue Net::ReadTimeout
        false
      end
    end
  end
end
