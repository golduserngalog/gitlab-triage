module StubAPI
  def stub_api(method, url, query: {}, body: {})
    response = yield if block_given?
    content_type = { 'Content-Type' => 'application/json' }

    WebMock::API.stub_request(method, url)
      .with(query: query, body: body, headers: content_type)
      .to_return(
        body: JSON.dump(response),
        headers: content_type.merge('X-Next-Page' => ''))
  end
end
