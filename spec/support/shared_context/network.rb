require 'gitlab/triage/network'
require 'gitlab/triage/network_adapters/httparty_adapter'

RSpec.shared_context 'network' do
  let(:net) do
    {
      host_url: 'http://test.com',
      api_version: 'v4',
      token: 'token',
      source_id: '1',
      network: network
    }
  end

  let(:network) do
    Gitlab::Triage::Network.new(
      Gitlab::Triage::NetworkAdapters::HttpartyAdapter.new)
  end

  before do
    allow(network).to receive(:print)
  end
end
