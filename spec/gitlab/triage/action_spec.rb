require 'spec_helper'

require 'gitlab/triage/action'

require 'active_support/core_ext/hash/except'
require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action do
  include_context 'network'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ].map(&:with_indifferent_access)
  end

  let(:action_args) do
    {
      name: 'Test rule name',
      type: 'issues',
      rule: action_rule,
      resources: resources,
      net: net
    }
  end

  let(:options) do
    {
      name: 'Test rule name',
      type: 'issues',
      rules: rules,
      resources: resources,
      net: net,
      dry: dry
    }
  end

  let(:action_rule) { rules }

  shared_examples 'acting with the right action' do |klass|
    context 'when doing actual run' do
      let(:dry) { false }

      it "uses #{klass} to process it" do
        expect_next_instance_of(klass, action_args) do |summarize|
          expect(summarize).to receive(:act)
        end

        subject.process(**options)
      end
    end

    context 'when doing dry-run' do
      let(:dry) { true }

      it "uses #{klass::Dry} to process it" do
        expect_next_instance_of(klass::Dry, action_args) do |summarize|
          expect(summarize).to receive(:act)
        end

        subject.process(**options)
      end
    end
  end

  describe '.process' do
    context 'when rules contain :summarize' do
      let(:rules) { { summarize: { title: 'title' } } }
      let(:action_rule) { rules[:summarize] }

      it_behaves_like 'acting with the right action',
        described_class::Summarize
    end

    context 'when rules contain :mention' do
      let(:rules) { { mention: ['user'] } }
      let(:action_rule) { rules }

      it_behaves_like 'acting with the right action',
        described_class::Comment
    end

    context 'when rules contain :summarize and :mention' do
      let(:rules) do
        {
          mention: ['user'],
          summarize: { title: 'title' }
        }
      end

      let(:summarize_args) { action_args.merge(rule: rules[:summarize]) }
      let(:comment_args) { action_args.merge(rule: rules.except(:summarize)) }

      context 'when doing actual run' do
        let(:dry) { false }

        it "uses Summarize and Comment to process it" do
          expect_next_instance_of(
            described_class::Summarize, summarize_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          expect_next_instance_of(
            described_class::Comment, comment_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          subject.process(**options)
        end
      end

      context 'when doing dry-run' do
        let(:dry) { true }

        it "uses Summarize::Dry and Comment::Dry to process it" do
          expect_next_instance_of(
            described_class::Summarize::Dry, summarize_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          expect_next_instance_of(
            described_class::Comment::Dry, comment_args) do |summarize|
            expect(summarize).to receive(:act)
          end

          subject.process(**options)
        end
      end
    end
  end
end
