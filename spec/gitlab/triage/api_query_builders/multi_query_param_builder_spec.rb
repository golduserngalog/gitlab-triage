require 'spec_helper'

require 'gitlab/triage/api_query_builders/multi_query_param_builder'

describe Gitlab::Triage::APIQueryBuilders::MultiQueryParamBuilder do
  let(:multi_params) do
    {
      'labels' => {
        param_content: %w[label1 label2],
        separator: ','
      }
    }
  end

  context '#build_param' do
    it 'should build the correct search string' do
      multi_params.each do |k, v|
        builder = described_class.new(k, v[:param_content], v[:separator])
        expect(builder.build_param).to eq("&#{k}=#{v[:param_content][0]}#{v[:separator]}#{v[:param_content][1]}")
      end
    end
  end
end
