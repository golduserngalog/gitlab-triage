require 'spec_helper'

require 'gitlab/triage/action/summarize/issue_builder'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::IssueBuilder do
  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ].map(&:with_indifferent_access)
  end

  let(:rule) do
    {
      title: title,
      item: item,
      summary: summary
    }
  end

  let(:title) { 'Issue title' }
  let(:item) { '- [ ] [{{title}}]({{web_url}})' }
  let(:summary) { "\# {{title}}\n\nHere's the summary:\n\n{{items}}" }
  let(:net) { {} }

  subject { described_class.new(rule, resources, net) }

  describe '#title' do
    it 'generates the correct issue title' do
      expect(subject.title).to eq(title)
    end
  end

  describe '#description' do
    it 'generates the correct issue description' do
      expect(subject.description).to eq(<<~TEXT.chomp)
        # Issue title

        Here's the summary:

        - [ ] [Issue #0](http://example.com/0)
        - [ ] [Issue #1](http://example.com/1)
      TEXT
    end

    context 'when there is no item' do
      let(:item) {}

      it 'generates a description with empty items' do
        expect(subject.description).to eq(<<~TEXT.chomp)
        # Issue title

        Here's the summary:


        TEXT
      end
    end

    context 'when there is no summary' do
      let(:summary) {}

      it 'generates an empty description' do
        expect(subject.description).to be_empty
      end
    end
  end

  describe '#valid?' do
    context 'when title is nil' do
      let(:title) {}

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is blank' do
      let(:title) { '  ' }

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is not blank' do
      let(:title) { 'title' }

      it 'is not valid' do
        expect(subject).to be_valid
      end
    end
  end
end
