require 'spec_helper'

require 'gitlab/triage/action/comment'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::Comment do
  include_context 'network'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ].map(&:with_indifferent_access)
  end

  let(:name) { 'Test comment rule' }
  let(:type) { 'issues' }

  let(:rule) do
    {
      mention: 'user',
      labels: ['label']
    }
  end

  let(:body) do
    <<~TEXT.chomp
      /label ~"label"

      /cc @user
    TEXT
  end

  subject do
    described_class.new(
      name: name, type: type, rule: rule, resources: resources, net: net)
  end

  describe '#act' do
    it 'posts the right issue' do
      stub_post = stub_api(
        :post,
        "http://test.com/api/v4/projects/1/issues/notes",
        body: { body: body }
      )

      subject.act

      assert_requested(stub_post, times: resources.size)
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end
  end
end
