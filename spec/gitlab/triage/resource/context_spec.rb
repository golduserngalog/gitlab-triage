require 'spec_helper'

require 'gitlab/triage/resource/context'

describe Gitlab::Triage::Resource::Context do
  include_context 'network'

  let(:resource) { {} }

  subject { described_class.new(resource, net) }

  describe '#eval' do
    it 'evaluate on the instance' do
      expect(subject).to receive(:milestone)

      subject.eval('milestone')
    end

    context 'when there is an internal error' do
      before do
        def subject.milestone
          raise NameError
        end
      end

      it 'captures the backtrace pointing to where the actual error raised' do
        expect { subject.eval('milestone') }
          .to raise_error(NameError) { |error|
            expect(error.backtrace.first).to include('milestone')
          }
      end
    end
  end

  describe '#milestone' do
    context 'when there is no milestone' do
      it 'gives something falsey' do
        expect(subject.__send__(:milestone)).to be_falsey
      end
    end

    context 'when there is a milestone' do
      let(:resource) { { milestone: { id: 1 } } }

      it 'gives the milestone based on the resource' do
        milestone = subject.__send__(:milestone)

        expect(milestone).to be_kind_of(Gitlab::Triage::Resource::Milestone)
        expect(milestone.id).to eq(1)
      end
    end
  end

  describe '#instance_version' do
    it 'gives an instance version object' do
      instance_version = subject.__send__(:instance_version)

      expect(instance_version)
        .to be_kind_of(Gitlab::Triage::Resource::InstanceVersion)
    end
  end
end
