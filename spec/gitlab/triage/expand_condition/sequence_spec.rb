require 'spec_helper'

require 'gitlab/triage/expand_condition/sequence'

describe Gitlab::Triage::ExpandCondition::Sequence do
  describe '.expand' do
    it 'expands the conditions for labels' do
      conditions = subject.expand(
        state: 'opened',
        labels: %w[missed:{0..1} missed:{2..3} missed]
      )

      expect(conditions).to eq(
        [
          { state: 'opened', labels: %w[missed:0 missed:2 missed] },
          { state: 'opened', labels: %w[missed:0 missed:3 missed] },
          { state: 'opened', labels: %w[missed:1 missed:2 missed] },
          { state: 'opened', labels: %w[missed:1 missed:3 missed] }
        ]
      )
    end
  end
end
